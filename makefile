CC=vc
LDFLAGS=-lamiga -lauto
FLAGS=
INC=-Iinclude
EXE=bin/dual
SRC=dual.c
	
UAE=D:\\media\\Software\\Emulation\\Amiga\\Harddrives\Applications\\dev\\dual\\

all: 
	@rmdir /s/q bin
	@mkdir bin\res
	$(CC) $(SRC) $(INC) $(LDFLAGS) $(FLAGS) -o $(EXE)	
	@rmdir /s/q $(UAE)
	@mkdir $(UAE)
	@xcopy /S/Q/Y res\\* bin\\res
	@xcopy /S/Q/Y bin\\* $(UAE)
