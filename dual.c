/**********************************************************************
 * Dual Playfield Demo
 * ROM Kernel Reference Manual - Libraries and Devices
 **********************************************************************/
#include <exec/types.h>
#include <hardware/dmabits.h>
#include <hardware/custom.h>
#include <hardware/cia.h>
#include <graphics/gfx.h>
#include <graphics/gfxbase.h>
#include <graphics/gfxmacros.h>
#include <graphics/rastport.h>
#include <graphics/view.h>
#include <exec/exec.h>

#include <clib/exec_protos.h>
#include <clib/dos_protos.h>
#include <clib/intuition_protos.h>
#include <clib/graphics_protos.h>

#include <stdio.h>
#include <stdlib.h>

#include "lib/iff.h"
#include <proto/iff.h>

#define DEPTH 3
#define WIDTH 320
#define HEIGHT 256
#define NOT_ENOUGH_MEMORY -1000

struct View v;
struct ViewPort vp1;

struct RasInfo ri1;
struct RasInfo ri2;

short i, j, k, n;
struct ColorMap *GetColorMap();
struct GfxBase *GfxBase;
struct IFFBase *IFFBase;
extern struct CIA ciaa, ciab;
struct View *oldview;

int rasScrollDir1 = 1;
int rasScrollDir2 = 2;

int FreeMemory();
void Scroll(void);
int MouseClick(void);

struct BitmapData
{
	// Color data
	struct ColorMap* cm;
	UWORD colortable[32];
	LONG colorTableSize;
	
	// The bitmap data
	UBYTE planes;
	struct BitMap *bitmap;
	
	struct RasInfo rasInfo;
};

struct BitmapData *LoadBitmap(char *filename, struct RasInfo *next);

int main(int argc, char **argv)
{
	GfxBase = (struct GfxBase *) OpenLibrary("graphics.library", 0);
	if (GfxBase == NULL) exit(1);

	IFFBase = (struct IFFBase *)OpenLibrary("iff.library", 0);
	if (IFFBase == NULL) exit(1);
	
	InitView(&v);
	v.ViewPort = &vp1;
	
	InitVPort(&vp1);
	vp1.DWidth = WIDTH;
	vp1.DHeight = HEIGHT;
	vp1.RasInfo = &ri1;
	vp1.Modes = DUALPF | PFBA;

	printf("InitBitMap 1\n");
	struct BitmapData *bd1 = LoadBitmap("res/gfx/drawBack.iff", NULL);
	ri1.BitMap = bd1->bitmap;
	ri1.RxOffset = 0;
	ri1.RyOffset = 0;
	
	printf("InitBitMap 2\n");
	struct BitmapData *bd2 = LoadBitmap("res/gfx/drawFront.iff", NULL);
	ri1.Next = &ri2;
	ri2.BitMap = bd2->bitmap;
	ri2.RxOffset = 0;
	ri2.RyOffset = 0;
	ri2.Next = NULL;

	printf("GetColorMap\n");
	
	vp1.ColorMap = bd1->cm;
	
	// Copy second half of colortable from first half of front layer
	memcpy(&bd1->colortable[8], &bd2->colortable[0], 16);
	
	// Load the color table
	LoadRGB4(&vp1, bd1->colortable, bd1->colorTableSize);
	
	printf("MakeVPort\n");
	MakeVPort(&v, &vp1);

	printf("MrgCop\n");
	MrgCop(&v);

	printf("LoadView");
	oldview = GfxBase->ActiView;
	LoadView(&v);
	
	printf("Wait\n");
    while (!MouseClick())
    {
		Scroll();
		WaitTOF();
    }
	
	printf("Unload");
	LoadView(oldview);
	WaitTOF();
	FreeMemory();
	
	CloseLibrary((struct Library *) GfxBase);
	CloseLibrary((struct Library *) IFFBase);

	return 0;
}


void Scroll(void)
{
	// 1
	if(ri1.RxOffset < 0)
	{
		rasScrollDir1 = 1;	
	}
	
	if(ri1.RxOffset > 320)
	{
		rasScrollDir1 = -1;
	}
	
	ri1.RxOffset+=rasScrollDir1;
	
	// 2
	if(ri2.RxOffset < 0)
	{
		rasScrollDir2 = 2;	
	}
	
	if(ri2.RxOffset > 960)
	{
		rasScrollDir2 = -2;
	}
	
	ri2.RxOffset+=rasScrollDir2;
	
	ScrollVPort(&vp1);
}

int FreeMemory()
{
  //FreeColorMap(cm);
  FreeVPortCopLists(&vp1);
  FreeCprList(v.LOFCprList);
  return 0;
}

int MouseClick(void)
{
    return !(ciaa.ciapra & CIAF_GAMEPORT0);
}

struct BitmapData *LoadBitmap(char *filename, struct RasInfo *next)
{	
	struct BitmapData *bitmapData = AllocMem(sizeof(struct BitmapData), MEMF_CHIP | MEMF_CLEAR);
	
	// Load background into bitmap
	printf("Load bitmap: %s\n",filename);
	IFFL_HANDLE iff = IFFL_OpenIFF(filename, IFFL_MODE_READ);
	if(iff == NULL)
	{
        printf("IFF open failed :(\n");
        return FALSE;
	}		

	struct IFFL_BMHD *bmhd = IFFL_GetBMHD(iff);
	if(bmhd==NULL)
	{
		printf("IFF is not bitmap :(\n");
        return FALSE;
	}
	
	bitmapData->cm = GetColorMap(16);
	if (bitmapData->cm == NULL)
	{
		printf("Could not get ColorMap\n");
        return FALSE;
	}
	
	// Load the background colour map
	bitmapData->colorTableSize = IFFL_GetColorTab(iff, bitmapData->colortable);	
	
	// Allocate the bitmap and decode it
	bitmapData->planes = bmhd->nPlanes;
	printf("Btmap: %dx%d @ %d planes\n",bmhd->w, bmhd->h, bmhd->nPlanes);
	bitmapData->bitmap = AllocBitMap(bmhd->w, bmhd->h, bmhd->nPlanes, BMF_CLEAR | BMF_DISPLAYABLE, NULL);
	if(!IFFL_DecodePic(iff, bitmapData->bitmap))
	{
		printf("Could not decode map bitmap\n");
        return FALSE;
	}
	
	bitmapData->rasInfo.BitMap = bitmapData->bitmap;
	bitmapData->rasInfo.RxOffset = 0;
	bitmapData->rasInfo.RyOffset = 0;
	bitmapData->rasInfo.Next = next;
	
	return bitmapData;
}